#!/usr/bin/python

from controllers.product_controller import ProductController
from controllers.checkout_controller import CheckoutController as Checkout
from models.product import Product
from models.bulk_purchase import BulkPurchase
from models.units_for_free import UnitsForFree


def run():
    ProductController.add_product(
        code='VOUCHER',
        name='Cabify Voucher',
        price=5
    )

    ProductController.add_product(
        code='TSHIRT',
        name='Cabify T-Shirt',
        price=20
    )

    ProductController.add_product(
        code='MUG',
        name='Cabify Coffee Mug',
        price=7.5
    )

    bulk_purchase_tshirt = BulkPurchase(
        product=ProductController.get_product_by_code('TSHIRT'),
        min_products=2,
        percentage_discount=5
    )

    units_for_free_voucher = UnitsForFree(
        product=ProductController.get_product_by_code('VOUCHER'),
        group_size=2,
        total_products_charged=1
    )

    pricing_rules = [bulk_purchase_tshirt, units_for_free_voucher]

    examples = [
        ['VOUCHER', 'TSHIRT', 'MUG'],
        ['VOUCHER', 'TSHIRT', 'VOUCHER'],
        ['TSHIRT', 'TSHIRT', 'TSHIRT', 'VOUCHER', 'TSHIRT'],
        ['VOUCHER', 'TSHIRT', 'VOUCHER', 'VOUCHER', 'MUG', 'TSHIRT', 'TSHIRT']
    ]
    for example_items in examples:
        checkout = Checkout(pricing_rules)
        for item in example_items:
            checkout.scan(item)
        print 'Items: \t{}'.format(example_items)
        print 'Total: \t{}\n'.format(checkout.total())

if __name__ == "__main__":
    run()
