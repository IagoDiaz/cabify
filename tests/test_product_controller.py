import unittest
from controllers.product_controller import ProductController, ProductDuplicate, TooManyProducts
from models.product import Product


class TestProductController(unittest.TestCase):

    def setUp(self):
        ProductController.clean()

    def test_add_product(self):
        ProductController.add_product(
            code='VOUCHER',
            name='Cabify Voucher',
            price=5
        )
        self.assertEqual(ProductController.get_total_products(), 1)

    def test_add_same_product(self):
        data = {
            'code': 'VOUCHER',
            'name': 'Cabify Voucher',
            'price': 5
        }
        ProductController.add_product(**data)
        self.assertRaises(ProductDuplicate, ProductController.add_product, **data)

    def test_add_bad_product(self):
        data = {
            'code': 3,
            'name': 'Cabify Voucher',
            'price': 5
        }
        self.assertRaises(ValueError, ProductController.add_product, **data)

        data = {
            'code': 'VOUCHER',
            'name': ['Cabify Voucher'],
            'price': 5
        }
        self.assertRaises(ValueError, ProductController.add_product, **data)

        data = {
            'code': 'VOUCHER',
            'name': 'Cabify Voucher',
            'price': '5'
        }
        self.assertRaises(ValueError, ProductController.add_product, **data)

    def test_too_many_products(self):
        products = [
            Product(
                code='VOUCHER',
                name='Cabify Voucher',
                price=5
            ),
            Product(
                code='VOUCHER',
                name='Cabify Voucher',
                price=5
            )
        ]
        ProductController.products = products
        self.assertRaises(TooManyProducts, ProductController.get_product_by_code, 'VOUCHER')
