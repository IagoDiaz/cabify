import unittest
from controllers.pricing_rules_controller import PricingRulesController
from models.product import Product
from models.bulk_purchase import RangeError


class TestPricingRulesController(unittest.TestCase):

    def setUp(self):
        PricingRulesController.clean()
        self.product = Product(
            code='VOUCHER',
            name='Cabify Voucher',
            price=5
        )

    def test_pricing_rules_bulk_purchase(self):
        PricingRulesController.add_bulk_purchase_pricing_rule(
            product=self.product,
            min_products=5,
            percentage_discount=20
        )
        self.assertEqual(
            PricingRulesController.pricing_rules[0].get_total_price(5),
            5 * self.product.get_price()
        )
        self.assertEqual(
            PricingRulesController.pricing_rules[0].get_total_price(6),
            6 * (self.product.get_price() * (100 - 20) / 100)
        )

    def test_pricing_rules_units_for_free_2x1(self):
        PricingRulesController.add_units_for_free_pricing_rule(
            product=self.product,
            group_size=2,
            total_products_charged=1
        )
        self.assertEqual(
            PricingRulesController.pricing_rules[0].get_total_price(5),
            2 * self.product.get_price() + self.product.get_price()
        )
        self.assertEqual(
            PricingRulesController.pricing_rules[0].get_total_price(6),
            3 * self.product.get_price()
        )

    def test_pricing_rules_units_for_free_3x2(self):
        PricingRulesController.add_units_for_free_pricing_rule(
            product=self.product,
            group_size=3,
            total_products_charged=2
        )
        self.assertEqual(
            PricingRulesController.pricing_rules[0].get_total_price(4),
            3 * self.product.get_price()
        )
        self.assertEqual(
            PricingRulesController.pricing_rules[0].get_total_price(5),
            4 * self.product.get_price()
        )
        self.assertEqual(
            PricingRulesController.pricing_rules[0].get_total_price(6),
            4 * self.product.get_price()
        )

    def test_pricing_rules_range_error(self):
        bulk_purchase_princing_rule_data = [
            {
                'product': self.product,
                'min_products': -2,
                'percentage_discount': 20
            },
            {
                'product': self.product,
                'min_products': 2,
                'percentage_discount': 120
            }
        ]
        for data in bulk_purchase_princing_rule_data:
            self.assertRaises(
                RangeError,
                PricingRulesController.add_bulk_purchase_pricing_rule,
                **data
            )

    def test_pricing_rules_value_error(self):
        bulk_purchase_princing_rule_data = [
            {
                'product': None,
                'min_products': 2,
                'percentage_discount': 20
            },
            {
                'product': self.product,
                'min_products': '2',
                'percentage_discount': 20
            },
            {
                'product': self.product,
                'min_products': 2,
                'percentage_discount': []
            }
        ]
        for data in bulk_purchase_princing_rule_data:
            self.assertRaises(
                ValueError,
                PricingRulesController.add_bulk_purchase_pricing_rule,
                **data
            )

        units_for_free_pricing_rule_data = [
            {
                'product': None,
                'group_size': 2,
                'total_products_charged': 1
            },
            {
                'product': self.product,
                'group_size': '2',
                'total_products_charged': 20
            },
            {
                'product': self.product,
                'group_size': 2,
                'total_products_charged': []
            },
            {
                'product': self.product,
                'group_size': 2,
                'total_products_charged': 3
            }
        ]
        for data in units_for_free_pricing_rule_data:
            self.assertRaises(
                ValueError,
                PricingRulesController.add_units_for_free_pricing_rule,
                **data
            )
