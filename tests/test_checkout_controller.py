import unittest
from controllers.product_controller import ProductController, ProductNotFound
from controllers.checkout_controller import CheckoutController as Checkout
from controllers.pricing_rules_controller import PricingRulesController
from models.product import Product
from models.bulk_purchase import BulkPurchase
from models.units_for_free import UnitsForFree


class TestCheckoutController(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._populate()

    def test_checkout_empty(self):
        checkout = Checkout(PricingRulesController.pricing_rules)
        self.assertEqual(checkout.total(), 0)

    def test_checkout_no_discounts(self):
        checkout = Checkout(PricingRulesController.pricing_rules)
        checkout.scan('VOUCHER')
        checkout.scan('TSHIRT')
        checkout.scan('MUG')
        self.assertEqual(checkout.total(), 32.5)

    def test_checkout_2x1_discount(self):
        checkout = Checkout(PricingRulesController.pricing_rules)
        checkout.scan('VOUCHER')
        checkout.scan('TSHIRT')
        checkout.scan('VOUCHER')
        self.assertEqual(checkout.total(), 25.0)

    def test_checkout_bulk_discount(self):
        checkout = Checkout(PricingRulesController.pricing_rules)
        checkout.scan('TSHIRT')
        checkout.scan('TSHIRT')
        checkout.scan('TSHIRT')
        checkout.scan('VOUCHER')
        checkout.scan('TSHIRT')
        self.assertEqual(checkout.total(), 81.0)

    def test_checkout_multiple_discounts(self):
        checkout = Checkout(PricingRulesController.pricing_rules)
        checkout.scan('VOUCHER')
        checkout.scan('TSHIRT')
        checkout.scan('VOUCHER')
        checkout.scan('VOUCHER')
        checkout.scan('MUG')
        checkout.scan('TSHIRT')
        checkout.scan('TSHIRT')
        self.assertEqual(checkout.total(), 74.5)

    def test_checkout_scan_no_product(self):
        checkout = Checkout(PricingRulesController.pricing_rules)
        self.assertRaises(ValueError, checkout.scan, 3)

    def test_checkout_scan_invalid_code(self):
        checkout = Checkout(PricingRulesController.pricing_rules)
        self.assertRaises(ProductNotFound, checkout.scan, 'CAP')

    @classmethod
    def _populate(self):
        ProductController.clean()
        PricingRulesController.clean()

        ProductController.add_product(
            code='VOUCHER',
            name='Cabify Voucher',
            price=5
        )

        ProductController.add_product(
            code='TSHIRT',
            name='Cabify T-Shirt',
            price=20
        )

        ProductController.add_product(
            code='MUG',
            name='Cabify Coffee Mug',
            price=7.5
        )

        PricingRulesController.add_bulk_purchase_pricing_rule(
            product=ProductController.get_product_by_code('TSHIRT'),
            min_products=2,
            percentage_discount=5
        )

        PricingRulesController.add_units_for_free_pricing_rule(
            product=ProductController.get_product_by_code('VOUCHER'),
            group_size=2,
            total_products_charged=1
        )
