_Tested under Python 2.7.10_

The examples cases read on challenge description can be run by doing:
```
$python main.py
```

```
Items: 	['VOUCHER', 'TSHIRT', 'MUG']
Total: 	32.5

Items: 	['VOUCHER', 'TSHIRT', 'VOUCHER']
Total: 	25

Items: 	['TSHIRT', 'TSHIRT', 'TSHIRT', 'VOUCHER', 'TSHIRT']
Total: 	81.0

Items: 	['VOUCHER', 'TSHIRT', 'VOUCHER', 'VOUCHER', 'MUG', 'TSHIRT', 'TSHIRT']
Total: 	74.5
```

To run tests, we simply need to run:
```
$python -m unittest discover -v tests
```

The result will be:
```
test_pricing_rules_bulk_purchase (test_pricing_rules_controller.TestPricingRulesController) ... ok
test_pricing_rules_range_error (test_pricing_rules_controller.TestPricingRulesController) ... ok
test_pricing_rules_units_for_free_2x1 (test_pricing_rules_controller.TestPricingRulesController) ... ok
test_pricing_rules_units_for_free_3x2 (test_pricing_rules_controller.TestPricingRulesController) ... ok
test_pricing_rules_value_error (test_pricing_rules_controller.TestPricingRulesController) ... ok
test_checkout_2x1_discount (test_checkout_controller.TestCheckoutController) ... ok
test_checkout_bulk_discount (test_checkout_controller.TestCheckoutController) ... ok
test_checkout_empty (test_checkout_controller.TestCheckoutController) ... ok
test_checkout_multiple_discounts (test_checkout_controller.TestCheckoutController) ... ok
test_checkout_no_discounts (test_checkout_controller.TestCheckoutController) ... ok
test_checkout_scan_invalid_code (test_checkout_controller.TestCheckoutController) ... ok
test_checkout_scan_no_product (test_checkout_controller.TestCheckoutController) ... ok
test_add_bad_product (test_product_controller.TestProductController) ... ok
test_add_product (test_product_controller.TestProductController) ... ok
test_add_same_product (test_product_controller.TestProductController) ... ok
test_too_many_products (test_product_controller.TestProductController) ... ok

----------------------------------------------------------------------
Ran 16 tests in 0.001s

OK
```

Coverage report looks like this:
```
Name                                      Stmts   Miss  Cover   Missing
-----------------------------------------------------------------------
controllers/__init__.py                       0      0   100%
controllers/checkout_controller.py           28      0   100%
controllers/pricing_rules_controller.py      12      0   100%
controllers/product_controller.py            29      0   100%
models/__init__.py                            0      0   100%
models/bases/__init__.py                      0      0   100%
models/bases/pricing_rule_base.py            10      1    90%   15
models/bulk_purchase.py                      22      0   100%
models/product.py                            12      0   100%
models/units_for_free.py                     16      0   100%
tests/test_checkout_controller.py            54      0   100%
tests/test_pricing_rules_controller.py       32      0   100%
tests/test_product_controller.py             24      0   100%
-----------------------------------------------------------------------
TOTAL                                       239      1    99%
```

In a real world project, models would be persisted into a database. These objects would possibly represent ORMs and its code would include the constraints they should fit.

Pricing rules can easily grow. We just need that they implement `get_total_price` method and the checkout controller will be able to use them.