from __future__ import division
from models.bases.pricing_rule_base import PricingRuleBase


class RangeError(Exception):
    pass


class BulkPurchase(PricingRuleBase):

    def __init__(self, product, min_products, percentage_discount):
        super(BulkPurchase, self).__init__(product)
        if not isinstance(min_products, int):
            raise ValueError()
        if not isinstance(percentage_discount, int):
            raise ValueError()
        if min_products < 0:
            raise RangeError("'min_products' should be positive")
        if percentage_discount < 0 or percentage_discount > 100:
            raise RangeError("'percentage_discount' should be between 0-100")

        self.min_products = min_products
        self.percentage_discount = percentage_discount
        self.product = product

    def get_total_price(self, total_products):
        if total_products > self.min_products:
            return total_products * self.product.get_price() * (100 - self.percentage_discount) / 100
        return total_products * self.product.get_price()
