from abc import ABCMeta, abstractmethod
from models.product import Product


class PricingRuleBase:
    __metaclass__ = ABCMeta

    def __init__(self, product):
        if not isinstance(product, Product):
            raise ValueError("'product' should be an instance of Product")
        self.product = product

    @abstractmethod
    def get_total_price(self, total_products):
        pass
