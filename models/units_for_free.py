from models.bases.pricing_rule_base import PricingRuleBase


class UnitsForFree(PricingRuleBase):

    def __init__(self, product, group_size, total_products_charged):
        super(UnitsForFree, self).__init__(product)
        if not isinstance(group_size, int):
            raise ValueError()
        if not isinstance(total_products_charged, int):
            raise ValueError()
        if not group_size > total_products_charged:
            raise ValueError("'group_size' can not be bigger than 'total_products_charged'")

        self.group_size = group_size
        self.total_products_charged = total_products_charged

    def get_total_price(self, total_products):
        total_charged = total_products / self.group_size * self.total_products_charged
        total_charged += total_products % self.group_size
        return total_charged * self.product.get_price()
