import numbers


class Product(object):

    def __init__(self, code, name, price):
        if not isinstance(code, str):
            raise ValueError()
        if not isinstance(name, str):
            raise ValueError()
        if not isinstance(price, numbers.Number):
            raise ValueError()
        self.code, self.name, self.price = code, name, price

    def get_price(self):
        return self.price
