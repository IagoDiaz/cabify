from models.bulk_purchase import BulkPurchase
from models.units_for_free import UnitsForFree


class PricingRulesController(object):

    pricing_rules = []

    @staticmethod
    def clean():
        PricingRulesController.pricing_rules = []

    @staticmethod
    def add_bulk_purchase_pricing_rule(product, min_products, percentage_discount):
        bulk_purchase = BulkPurchase(
            product=product,
            min_products=min_products,
            percentage_discount=percentage_discount
        )
        PricingRulesController.pricing_rules.append(bulk_purchase)

    @staticmethod
    def add_units_for_free_pricing_rule(product, group_size, total_products_charged):
        units_for_free = UnitsForFree(
            product=product,
            group_size=group_size,
            total_products_charged=total_products_charged
        )
        PricingRulesController.pricing_rules.append(units_for_free)
