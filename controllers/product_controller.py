import numbers
from models.product import Product


class ProductNotFound(Exception):
    def __init__(self, code):
        super(ProductNotFound, self).__init__("product code: '{}' is not registered".format(code))


class ProductDuplicate(Exception):
    def __init__(self, code):
        super(ProductDuplicate, self).__init__("product code: '{}' is already registered".format(code))


class TooManyProducts(Exception):
    def __init__(self, code):
        super(TooManyProducts, self).__init__("There are more that on product with product code: '{}'".format(code))


class ProductController(object):

    products = []

    @staticmethod
    def add_product(code, name, price):
        product = Product(
            code=code,
            name=name,
            price=price
        )

        if len(filter(lambda product: product.code == code, ProductController.products)) == 1:
            raise ProductDuplicate(code)

        ProductController.products.append(product)

    @staticmethod
    def clean():
        ProductController.products = []

    @staticmethod
    def get_total_products():
        return len(ProductController.products)

    @staticmethod
    def get_product_by_code(code):
        products = filter(lambda product: product.code == code, ProductController.products)
        if len(products) == 1:
            return products[0]
        elif len(products) > 1:
            raise TooManyProducts(code)
        raise ProductNotFound(code)
