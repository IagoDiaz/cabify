from controllers.product_controller import ProductController, ProductNotFound


class CheckoutController(object):

    def __init__(self, pricing_rules):
        self.pricing_rules = pricing_rules
        self.products = {}

    def scan(self, product_code):
        if not isinstance(product_code, str):
            raise ValueError("You must scan a 'product code'")

        product = ProductController.get_product_by_code(product_code)

        if product_code in self.products:
            self.products[product.code] += 1
        else:
            self.products[product.code] = 1

    def get_pricing_rule_for_product_code(self, code):
        for rule in self.pricing_rules:
            if rule.product.code == code:
                return rule
        return None

    def get_price_by_code(self, code):
        product = ProductController.get_product_by_code(code)
        return product.price if product else 0

    def total(self):
        total = 0
        for code, quantity in self.products.iteritems():
            pricing_rule = self.get_pricing_rule_for_product_code(code)
            if pricing_rule:
                total += pricing_rule.get_total_price(quantity)
            else:
                total += self.get_price_by_code(code) * quantity
        return total
